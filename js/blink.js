function blink(selector) {
    $(selector).fadeOut('slow', function() {
        $(this).fadeIn('slow', function() {
            blink(this);
        });
    });
}
$("#imgMonth").attr("src", "../../img/gifs/" + (new Date().getMonth() + 1) + ".gif");
$("#aMonth").attr("href", "../../img/gifs/" + (new Date().getMonth() + 1) + ".gif");
blink('.blink');